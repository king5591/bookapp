import { BooksComponent } from "./books/books.component";
import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";

const routes: Routes = [
  { path: "books", component: BooksComponent },
  { path: "books/:genre", component: BooksComponent },
  { path: "**", redirectTo: "books", pathMatch: "full" },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
