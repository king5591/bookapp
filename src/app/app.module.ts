import { HttpClientModule } from "@angular/common/http";
import { NetService } from "./net.service";
import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";
import { NgbModule } from "@ng-bootstrap/ng-bootstrap";
import { FormsModule } from "@angular/forms";

import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { BooksComponent } from "./books/books.component";
import { LeftPanelComponent } from "./left-panel/left-panel.component";

@NgModule({
  declarations: [AppComponent, BooksComponent, LeftPanelComponent],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NgbModule,
    FormsModule,
    HttpClientModule,
  ],
  providers: [NetService],
  bootstrap: [AppComponent],
})
export class AppModule {}
