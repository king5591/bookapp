import { Component, OnInit, Input, Output, EventEmitter } from "@angular/core";

@Component({
  selector: "app-left-panel",
  templateUrl: "./left-panel.component.html",
  styleUrls: ["./left-panel.component.css"],
})
export class LeftPanelComponent implements OnInit {
  @Input() bestSeller;
  @Input() language;
  @Output() optSel1 = new EventEmitter();
  constructor() {}

  ngOnInit() {}
  emitChange() {
    this.optSel1.emit();
  }
}
