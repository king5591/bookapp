export class Option {
  totalNum: number;
  refineValue: string;
  isSelected: boolean;
}

export class AllOptions {
  options: Option[];
  display: string;
  selected: string;
}
