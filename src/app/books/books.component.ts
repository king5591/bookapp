import { AllOptions } from "./../options";
import { NetService } from "./../net.service";
import { Component, OnInit } from "@angular/core";
import { Router, ActivatedRoute, ParamMap } from "@angular/router";

@Component({
  selector: "app-books",
  templateUrl: "./books.component.html",
  styleUrls: ["./books.component.css"],
})
export class BooksComponent implements OnInit {
  url = "http://localhost:2410/booksApp/books";
  genreUrl = "http://localhost:2410/booksApp/books/";
  newUrl = "http://localhost:2410/booksApp/books?newarrival=Yes&page=";
  bestSellerOptions: AllOptions;
  languageOptions: AllOptions;
  data;
  newarrival: string = null;
  genre: string = null;
  n: number = 1;
  page: string = "1";
  item: number = 1;
  numOfItem: number = 10;
  isSelected = "";
  language = "";
  bestSeller = "";

  constructor(
    private netService: NetService,
    private route: ActivatedRoute,
    private router: Router
  ) {}

  ngOnInit() {
    this.netService.getData(this.url).subscribe((resp) => {
      this.data = resp;
      // console.log(this.newarrival);
    });
    this.route.paramMap.subscribe((param) => {
      this.genre = param.get("genre");
      if (this.genre) {
        this.netService
          .getData(this.genreUrl + this.genre)
          .subscribe((resp) => {
            this.data = resp;
            this.page = "1";
            this.item = 1;
            this.numOfItem = 10;
            this.n = 1;
          });
      }
    });
    this.route.queryParamMap.subscribe((param) => {
      // console.log(this.genre);
      this.newarrival = param.get("newarrival");
      this.page = param.get("page");
      if (this.newarrival) {
        if (this.page == null) {
          this.page = "1";
          this.item = 1;
          this.numOfItem = 10;
          this.n = 1;
        }
        this.netService.getData(this.newUrl + this.page).subscribe((resp) => {
          this.data = resp;
          this.makeStructure();
        });
      } else if (this.genre) {
        // console.log(this.genre);
        this.netService
          .getData(this.genreUrl + this.genre + "?page=" + this.page)
          .subscribe((resp) => {
            this.data = resp;
            this.makeStructure();
          });
      } else {
        if (this.language && this.bestSeller) {
          console.log(this.bestSeller);
          this.netService
            .getData(
              this.url +
                "?language=" +
                this.language +
                "&bestseller=" +
                this.bestSeller +
                "&page=" +
                this.page
            )
            .subscribe((resp) => {
              this.data = resp;
              this.makeStructure();
              let index = this.bestSellerOptions.options.findIndex(
                (l1) => l1.refineValue == this.bestSeller
              );
              let best1 = this.bestSeller.split(",");
              for (let i = 0; i < best1.length; i++) {
                let item = this.bestSellerOptions.options.find(
                  (b1) => b1.refineValue == best1[i]
                );
                if (item) {
                  item.isSelected = true;
                }
              }
              this.bestSellerOptions.selected = this.bestSeller;
              let index1 = this.languageOptions.options.findIndex(
                (l1) => l1.refineValue == this.language
              );
              let lang1 = this.language.split(",");
              for (let i = 0; i < lang1.length; i++) {
                let item = this.languageOptions.options.find(
                  (l1) => l1.refineValue == lang1[i]
                );
                if (item) {
                  item.isSelected = true;
                }
              }
              this.languageOptions.selected = this.language;
            });
        } else if (this.language) {
          this.netService
            .getData(
              this.url + "?language=" + this.language + "&page=" + this.page
            )
            .subscribe((resp) => {
              this.data = resp;
              this.makeStructure();
              let index = this.languageOptions.options.findIndex(
                (l1) => l1.refineValue == this.language
              );
              let lang1 = this.language.split(",");
              for (let i = 0; i < lang1.length; i++) {
                let item = this.languageOptions.options.find(
                  (l1) => l1.refineValue == lang1[i]
                );
                if (item) {
                  item.isSelected = true;
                }
              }
              this.languageOptions.selected = this.language;
            });
        } else if (this.bestSeller) {
          this.netService
            .getData(
              this.url + "?bestseller=" + this.bestSeller + "&page=" + this.page
            )
            .subscribe((resp) => {
              this.data = resp;
              this.makeStructure();
              let index = this.bestSellerOptions.options.findIndex(
                (l1) => l1.refineValue == this.bestSeller
              );
              let best1 = this.bestSeller.split(",");
              for (let i = 0; i < best1.length; i++) {
                let item = this.bestSellerOptions.options.find(
                  (b1) => b1.refineValue == best1[i]
                );
                if (item) {
                  item.isSelected = true;
                }
              }
              this.bestSellerOptions.selected = this.bestSeller;
            });
        } else {
          this.netService
            .getData(this.url + "?page=" + this.page)
            .subscribe((resp) => {
              this.data = resp;
              this.makeStructure();
            });
        }
      }
    });
  }
  makeStructure() {
    this.language = this.language ? this.language : "";
    this.bestSeller = this.bestSeller ? this.bestSeller : "";
    let best = this.data.refineOptions.bestseller.map((b1) => ({
      totalNum: b1.totalNum,
      refineValue: b1.refineValue,
      isSelected: false,
    }));
    let lang = this.data.refineOptions.language.map((l1) => ({
      totalNum: l1.totalNum,
      refineValue: l1.refineValue,
      isSelected: false,
    }));
    this.bestSellerOptions = {
      options: best,
      display: "Bestseller",
      selected: "",
    };
    this.languageOptions = {
      options: lang,
      display: "Language",
      selected: "",
    };
    let lang1 = this.language.split(",");
    for (let i = 0; i < lang1.length; i++) {
      let item = this.languageOptions.options.find(
        (l1) => l1.refineValue == lang1[i]
      );
      if (item) {
        item.isSelected = true;
      }
    }
    let best1 = this.bestSeller.split(",");
    for (let i = 0; i < best1.length; i++) {
      let item = this.bestSellerOptions.options.find(
        (b1) => b1.refineValue == best1[i]
      );
      if (item) {
        item.isSelected = true;
      }
    }
    // console.log(this.bestSellerOptions);
  }

  optChange() {
    this.page = "1";
    this.item = 1;
    this.numOfItem = 10;
    this.n = 1;
    let path = "books";
    let qparam = {};
    let option2 = this.languageOptions.options.filter((l1) => l1.isSelected);
    let lang1 = option2.map((l1) => l1.refineValue);
    this.language = lang1.join(",");
    if (option2.length) {
      this.languageOptions.selected = this.language;
      qparam["language"] = this.language;
    } else {
      this.languageOptions.selected = "";
    }
    let option1 = this.bestSellerOptions.options.filter((b1) => b1.isSelected);
    let best = option1.map((b1) => b1.refineValue);
    this.bestSeller = best.join(",");
    if (option1.length) {
      this.bestSellerOptions.selected = this.bestSeller;
      qparam["bestseller"] = this.bestSeller;
    } else {
      this.bestSellerOptions.selected = "";
    }
    this.router.navigate([path], { queryParams: qparam });
    console.log(this.languageOptions.selected);
  }

  next() {
    this.n++;
    this.page = "" + this.n;
    let path = "books/";
    let qparam = {};
    if (this.newarrival) {
      // console.log(this.newarrival);
      qparam["newarrival"] = this.newarrival;
      qparam["page"] = this.page;
    } else if (this.genre) {
      path = path + this.genre;
      // console.log(this.genre);
      qparam["page"] = this.page;
    } else {
      qparam["language"] = this.language;
      qparam["bestselle"] = this.bestSeller;
      qparam["page"] = this.page;
    }
    this.item += this.data.pageInfo.numOfItems;
    this.numOfItem += this.data.pageInfo.numOfItems;
    if (this.numOfItem > this.data.pageInfo.totalItemCount) {
      this.numOfItem -= this.data.pageInfo.numOfItems;
      this.numOfItem += this.data.pageInfo.totalItemCount - this.item + 1;
    }
    this.router.navigate([path], { queryParams: qparam });
  }

  previous() {
    this.n--;
    this.page = "" + this.n;
    let path = "books/";
    let qparam = {};
    if (this.newarrival) {
      // console.log(this.newarrival);
      qparam["newarrival"] = this.newarrival;
      qparam["page"] = this.page;
    } else if (this.genre) {
      path = path + this.genre;
      // console.log(this.genre);
      qparam["page"] = this.page;
    } else {
      qparam["page"] = this.page;
    }
    if (this.numOfItem == this.data.pageInfo.totalItemCount) {
      this.numOfItem -= this.data.pageInfo.totalItemCount - this.item + 1;
    } else {
      this.numOfItem -= this.data.pageInfo.numOfItems;
    }
    this.item -= this.data.pageInfo.numOfItems;

    this.router.navigate([path], { queryParams: qparam });
  }
}
